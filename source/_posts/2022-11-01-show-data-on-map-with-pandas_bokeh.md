---
title: 使用pandas_bokeh在地图上显示数据
date: 2022-11-01 09:31:00 +0800
categories: python
tags: [map, pandas_bokeh]
excerpt: 本文记录阿猪使用pandas_bokeh的踩坑过程，供大家参考。
cover: /post_img/2022/11/pandas_bokeh-01.gif
---

　　本文记录阿猪使用pandas_bokeh的踩坑过程，供大家参考。
## 一、简介
　　pandas_bokeh集成了bokeh、geopandas等模块，除了可以绘制各类图表，还支持直接在地图上显示数据。效果举例如下：
<div><img src="/post_img/2022/11/pandas_bokeh-01.gif" alt="select layers" width="512" height=auto/></div>

<div><img src="/post_img/2022/11/pandas_bokeh-02.gif" alt="slide with different time series" width="512" height=auto/></div>

<div><img src="/post_img/2022/11/pandas_bokeh-03.gif" alt="different layers" width="512" height=auto/></div>
　　<a href ="https://github.com/PatrikHlobil/Pandas-Bokeh" target="_blank">详细介绍和具体用法请参考GitHub - Pandas-Bokeh</a>

## 二、数据文件
　　pandas_bokeh的MapPlots方法只需要点坐标即可定位，支持直接从csv等常规文件中读取数据。但是功能更丰富的GeoPlots方法是基于GeoJson格式的数据的，需要从GeoJson文件中读取位置数据，如果直接向dataframe写入位置数据，则pandas_bokeh会在读取数据的时候报错`Type Error: Unhashable type: 'list'`。
　　对于type类型为Feature的GeoJson文件，官方给出的基本结构示例如下：

```json
{
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  },
  "properties": {
    "name": "Dinagat Islands"
  }
}
```
　　一级key包含type、geometry、properties。其中geometry中存放位置信息，包含type、coordinates两个key。type包含Point(点)、Polygon(多个点连接而成的多边形)、Multipolygon(多个Polygon，比如印尼有很多个岛屿，存储其国土形状信息就需要多个Polygon)。properties存放其他各类数据。 

　　对于type类型为FeatureCollection的GeoJson文件，阿猪总结的基本结构示例如下：
```json
{
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
				{ 
				"type": "Feature", 
				"properties": 
					{ 
					"FIPS": "AC", 
					"ISO2": "AG", 
					"ISO3": "ATG", 
					"UN": 28, 
					"NAME": "Afghanistan", 
					"AREA": 44, 
					"POP2005": 83039, 
					"POP2006": 93039,
					"REGION": 19, 
					"SUBREGION": 29, 
					"LON": -61.783, 
					"LAT": 17.078 
					}, 
				"geometry": { 
								"type": "MultiPolygon", 
								"coordinates": [ [ [ [ -61.686668, 17.024441000000152 ], [ -61.887222, 17.105274 ], [ -61.794449, 17.163330000000101 ], [ -61.686668, 17.024441000000152 ] ] ], [ [ [ -61.729171999999892, 17.608608 ], [ -61.853058, 17.583054000000104 ], [ -61.873062, 17.703888 ], [ -61.729171999999892, 17.608608 ] ] ] ] } 
				},
 
				{ "type": "Feature", "properties": { "FIPS": "AL", "ISO2": "AL", "ISO3": "ALB", "UN": 8, "NAME": "Albania", "AREA": 2740, "POP2005": 3153731, "POP2006": 4153731, "REGION": 150, "SUBREGION": 39, "LON": 20.068, "LAT": 41.143 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 19.436214, 41.021065 ], [ 19.600555, 41.796661 ], [ 19.367771, 41.848999 ], [ 19.645832, 42.61805 ], [ 20.071423, 42.560913 ], [ 20.589642, 41.882187 ], [ 20.492775, 41.331108 ], [ 20.82111, 40.908882 ], [ 20.98349, 40.855888 ], [ 20.671944, 40.098053000000121 ], [ 20.010029, 39.6912 ], [ 19.863052, 40.039719 ], [ 19.288609, 40.417496 ], [ 19.478611, 40.350273 ], [ 19.436214, 41.021065 ] ] ] } }
]
 
}
```
　　一级key一般包含type、crs、features。猜测crs应该是声明features中keys、values的结构的。features的数据的形式类似于list，可以根据自己的需要放入一个或多个嵌套的类似dict结构的数据。比如你需要画5个国家，显示每个国家的人口数据，那么就对应5个。

　　<b>这里是重点</b>，你想要显示在地图上的数据，都需要放到feature下每个嵌套"dict"的properties中。例如上边示例中的POP2005、POP2006就是自己加进去的。Pandas_Bokeh也是在properties里寻找你要它显示的数据。

　　Pandas_Bokeh读取完GeoJson数据后，是以DataFrame形式存放的。所以如果你嫌弃在GeoJson文件中写入不方便，理论上也可以使用传统的csv文件、数据库等方式储存自己的数据，然后在这个DataFrame中增加一列或多列，把你自己的数据放入这个DataFrame。

## 三、参考信息
　　关于位置信息的数据，以下几个网站供参考：

### (1)<a href ="https://thematicmapping.org/downloads/world_borders.php" target="_blank"> World Boarders - TheMaticMapping.org</a>

　　一个不明觉厉的网站，提供World Borders Dataset的shapefile下载，自行转换为GeoJson格式后即可使用。可以通过Pandas_Bokeh在地图上轻松绘制每个国家的形状。

　　可以使用geopandas实现shp格式向geojson格式的转换。

```python
import geopandas
data = geopandas.read_file('shp文件的路径')
data.to_file('保存结果的文件路径', driver="GeoJSON", encoding='utf-8')
```

### (2)<a href ="http://datav.aliyun.com/portal/school/atlas/area_selector" target="_blank"> DataV.GeoAtlas DataV.GeoAtlas地理小工具系列 (aliyun.com)</a>
　　阿里云提供的一个在线小工具，包括范围选择器、边界生成器、层级生成器。