---
title: 如何下载旧版本Python的安装包
date: 2023-07-23 22:00:00 +0800
categories: python
tags: [installer]
cover: /post_img/general/python.webp
---
　　本文介绍如何从Python官网中找到旧版本Python安装包的下载链接。<!-- more -->

　　受限于一些第三方模块对新版本Python的支持问题，我们有时候不得不使用较旧版本的Python。但是Python官方对旧版本的支持通常是有限的，当你想在官网重新下载某个旧版本的安装文件时，会发现只能在Download页面找到它的security release或者bugfix release，而安装包已经找不到了。

　　以阿猪要找的Python3.9为例，在Download页面只能看到Python3.11的安装包下载链接，而Python3.9只能看到一个security release的下载链接。
<div style="text-align: center;"><img src="/post_img/2023/07/installer-for-older-versions-of-python-01.jpg" alt="" style="width: 100%; height: auto; max-width: 700px;"></div>

　　点击这个security release的链接，可以在`Files`这里看到只有安全补丁的下载链接，没有安装包的下载链接。在`No installers`这里可以看到“...Python3.9目前进入‘仅提供安全补丁’的阶段...二进制安装程序已经不再提供...Python3.9.13是最后一个包含了二进制安装程序的bugfix版本”的提示。
<div style="text-align: center;"><img src="/post_img/2023/07/installer-for-older-versions-of-python-02.jpg" alt="" style="width: 100%; height: auto; max-width: 700px;"></div>
　　答案就在这里了，在Python3.9.x中，3.9.13是最后一个包含了安装程序的bugfix版本。

　　回到Download页面，在`Looking for a special release?`这里向下拉动滚动条，找到前边提到的3.9.13，点击进入。
<div style="text-align: center;"><img src="/post_img/2023/07/installer-for-older-versions-of-python-03.jpg" alt="" style="width: 100%; height: auto; max-width: 700px;"></div>
将页面拉动至底部`Files`区域，就可以看到亲切的安装包下载链接了。
<div style="text-align: center;"><img src="/post_img/2023/07/installer-for-older-versions-of-python-04.jpg" alt="" style="width: 100%; height: auto; max-width: 700px;"></div>