---
title: python中使用'''注释代码后引起报错
date: 2023-01-25 18:31:00 +0800
categories: python
tags: [syntaxerror, comment]
cover: /post_img/general/bug.png
excerpt: 如果被注释的代码存在转义冲突，则会引起报错。可以在注释符号之前添加一个`r`避免转义。
---

　　`'''`是python中常用的代码注释符号，只需在代码段的开头结尾处分别插入`'''`即可实现跨行注释，很方便。

　　但是如果被注释的代码段中包含存在转义歧义的内容，则可能引起类似报错：`SyntaxError: (unicode error) 'unicodeescape' codec can't decode bytes in position 45-46: truncated \UXXXXXXXX escape`

　　此时你可以尝试将开头的`'''`改为`r'''`，即声明整个注释的代码不进行转义。
