---
title: 踩坑阿里云函数计算搭建WordPress
date: 2023-04-28 15:30:00 +0800
updated: 2023-06-30 15:30:00 +0800
categories: web
tags: [aliyun, serverless, wordpress]
excerpt: 本文记录阿猪使用阿里云的函数计算服务搭建WordPress的一些采坑过程。
---
　　阿里云在社区应用中提供了现成的模板供用户快速创建一个基于函数计算+NAS存储的WordPress站点，服务器环境是基于Nginx+php，数据库是基于sqlite。本文记录阿猪使用阿里云的函数计算服务搭建WordPress的一些采坑过程。

## 一、部署站点
　　阿里云提供了命令行工具和在线部署两种部署方法，两种方法的核心原理是一样的。
<h3><b>方法1：使用s工具</b></h3>
　　对于喜欢使用命令行工具的用户，可以使用阿里云提供的serverless-devs工具(简称“s工具”)。具体教程请参考这里：<a href="https://developer.aliyun.com/adc/scenario/b6afe83469b74fd681e22c48c15f3371" target="_blank">基于函数计算快速搭建WordPress博客系统</a>

　　请注意，教程中的服务器环境并不是必须的，你可以使用自己的本地Linux或者Windows环境完成同样的任务。
<h3><b>方法2：在线部署</b></h3>
　　阿里云也提供了更为简便的在线部署方法（也称“白屏化”方法），<a href="https://fcnext.console.aliyun.com/applications/create?template=start-wordpress" target="_blank">点击这里可以直接开始创建一个WordPress应用。</a>

## 二、环境优化
### 1、添加写权限
　　使用s工具中的模板安装完成后，会遇到WordPress无法通过管理后台升级、安装插件、模板的问题。这是因为缺少WordPress所在目录的写权限。
　　解决办法：打开函数计算的管理后台，在`服务与函数`列表中找到WordPress对应的服务。点击名称后进入该服务对应的`函数管理`列表，然后点击`wordpress`(默认名称)进入WebIDE。
<div align="center"><img src="/post_img/2023/04/serverless-01.jpg" alt="" width="512" height=auto/></div>
　　在Web IDE中打开`start.sh`文件，在如图所示的权限配置代码的下方插入如下代码，然后保存并点击“部署”：

``` bash
chown -R www-data /mnt/auto/wordpress
chmod -R 775 /mnt/auto/wordpress
```
　　注：以上代码中使用的是教程中的模板生成的默认路径。

### 2、添加SSL支持

　　为部署好的站点安装SSL证书后，发现WordPress前台可以访问，但是wp-admin后台无法访问。这是因为没有给WordPress开启https。

　　解决办法：打开WordPress目录下的wp-config.php，在适当的位置插入如下代码：
```php
$_SERVER['HTTPS'] = 'on';
define('FORCE_SSL_LOGIN', true);
define('FORCE_SSL_ADMIN', true);
```

### 3、优化URL Rewrite规则

　　使用在线部署方法部署的WordPress站点，在访问robots.txt、wp-sitemap.xml文件时，会发现虽然页面可以正常显示，但是HTTP Header返回的状态码确是404，这会导致搜索引擎无法正常对站点进行索引。

　　这是因为在WordPress中，robots.txt、wp-sitemap.xml这类文件是虚拟的文件，并不存在真实的物理路径，需要在Nginx中正确配置Rewrite规则才能正常访问。而使用在线部署方法部署的WordPress站点，其生成的Nginx.conf配置文件中有些规则上的冲突，导致Nginx在响应`https://yfzhu.cn/robots.txt`这种包含后缀的伪静态URL时会错误的返回404状态。

　　解决办法：参照前边的方法打开Web IDE，在Web IDE中打开`nginx.conf`文件，按照如下步骤修改：
（1）在第53行的开头插入一个`#`，将该行注释掉：
``` bash
# location = /robots.txt  { access_log off; log_not_found off; }
```
（2）将第69行的`try_files $uri =404;`修改为：
``` bash
try_files $uri $uri/ /index.php?$args;
```
　　解决冲突的方法并不唯一，使用这个方法暂时没有发现引起新的问题。

### 4、优化访问速度

　　经过多次测试，阿猪发现serverless和ECS之类的服务器在ping的速度上没有明显差异，但是在服务器渲染页面的速度上会相差1秒左右。
<div align="center"><img src="/post_img/2023/04/serverless-02.jpg" alt="" width="512" height=auto/></div>
　　解决办法：放弃serverless，花钱买ECS服务器

　　缓解办法：
　　（1）适当提高函数的配置并限制并发数。建议配置不低于1核1GB，不高于2核2GB，并发数不高于15。
　　（2）为WordPress安装WP Super Cache之类的静态缓存插件，将页面提前预缓存为html文件，从而省去每次渲染页面的时间。

## 三、管理NAS里的文件
### 1、挂载NAS到服务器
　　把NAS挂载到阿里云的ECS上，通过`/mnt/<挂载目录>`访问，这要求NAS和ECS在同一个地域。比如都在杭州、都在北京。如果不在同一个地域，需要在`专有网络`中使用`云企业网`创建一个跨地域的虚拟内网。（注：使用这个服务会产生费用）
　　这个方法虽然不方便，但是在使用体验上等同于在原生服务器上读写文件，适合量大、复杂的文件操作。
### 2、使用可道云在线管理文件
　　使用函数计算部署一个可道云应用，然后挂载同一个NAS，便可使用可道云对NAS内的文件进行web可视化管理。参见教程：<a href="https://help.aliyun.com/document_detail/444588.html" target="_blank">使用函数计算快速搭建管理NAS的可视化应用</a>
### 3、在WordPress中安装ftp插件
　　这个方法受服务器限制大，只适合简单的文件编辑和少量文件的上传下载。
### 4、使用专业版WebIDE
　　专业版WebIDE本质上是一个挂载了NAS的函数计算服务，可以直接访问挂载的NAS。（注：专业版WebIDE使用了函数计算、NAS这两项服务，会产生费用）
