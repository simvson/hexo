---
title: 如何为不同的Python项目自动选择不同的解释器
date: 2023-07-14 22:30:00 +0800
categories: python
tags: [vscode, workspace, interpreter]
cover: /post_img/general/vs-code.jpg
---
　　本文介绍在Windows下通过为特定的Python项目创建VS Code工作空间，从而实现为不同的Python项目自动选择不同的解释器。<!-- more -->

## 一、需求场景
　　使用高版本的Python通常意味着更好的性能、新的特性、BUG的修复，但是受限于一些第三方库对新版本的支持问题，有时候不得不保留较旧版本的Python。这会产生一个小麻烦，即每次打开使用了不同解释器的项目时，都需要手动去重新选择Python解释器。
　　阿猪尝试了通过度娘、谷哥、ChatGPT找到的各种方法，包括在PY文件的头部添加shebang语句、在项目的.vscode文件夹内创建/修改launch.json、settings.json文件......，但都不起作用或者不满足需求。好在最后ChatGPT超常发挥，提供了一个“创建工作空间”的靠谱方法。

## 二、操作步骤
　　这里假设你的VS Code全局默认的Python解释器为Python3.11，A项目因为特殊原因需要使用Python3.8。
### 1、在VS Code中打开项目所在目录
　　首先打开A项目的根目录，在空白处点击鼠标右键，然后选择“通过Code打开”。此时VS Code会在内置的资源管理器中打开这个文件夹。

　　如果VS Code询问你“是否信任此文件夹中的文件的作者”，点击“信任”即可。

### 2、将文件夹添加到工作区
　　在VS Code左侧资源管理器界面的空白区域点击鼠标右键，然后选择“将文件夹添加到工作区”。接着在弹出的窗口中选择该项目的文件夹，并为工作区起一个名字，然后点击“添加”按钮。

### 3、为该工作区单独设置解释器
　　在VS Code顶部的菜单栏中依次点击“查看”-“命令面板”，然后选择或者手动输入`>Python:选择解释器`，然后选择你需要的Python解释器即可。
<div style="text-align: center;"><img src="/post_img/2023/07/set-python-interpreter-01.jpg" alt="" style="width: 100%; height: auto; max-width: 700px;"></div>

　　如果弹出`命令"Python:选择解释器"导致错误`的报错提示框，一般是因为你没有将该文件夹设置为信任。
<div style="text-align: center;"><img src="/post_img/2023/07/set-python-interpreter-02.jpg" alt="" style="width: 100%; height: auto; max-width: 400px;"></div>
　　为工作空间设置好解释器后，点击VS Code窗口右上角的“运行”按钮测试一下，通过终端输出的内容可以看出VS Code已经在使用新设置的解释器工作了。
<div style="text-align: center;"><img src="/post_img/2023/07/set-python-interpreter-03.jpg" alt="" style="width: 100%; height: auto;"></div>

### 4、另存为工作区文件
　　此时虽然我们已经为这个新创建的工作区单独设置了解释器，但是下次仍然需要在VS Code的资源管理器中打开项目所在的文件夹，并从这里运行PY文件才能有效。否则如果直接在Windows的资源管理器中打开PY文件，仍然会调用VS Code全局默认的Python解释器。这个操作步骤还是有些烦人的。

　　我们可以将该工作区另存为工作区文件，方便下次直接打开该工作区。

　　在VS Code顶部的菜单栏中依次点击“文件”-“将工作区另存为”，然后在弹出的窗口中将工作区文件以code-workspace为后缀名保存在项目的根目录即可。

　　在Windows资源管理器中双击刚才保存的code-workspace文件，可以看到VS Code直接打开了先前创建的工作区。点击VS Code右上角的“运行”按钮测试一下，通过终端输出的内容可以看出VS Code使用的是为该工作区单独设置的解释器。继续测试运行该工作区之外的PY文件，通过终端输出的内容可以看出VS Code在刚才的工作区之外使用的还是全局默认的解释器。大功告成！


