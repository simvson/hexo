---
title: 在AWS Amplify中部署Jekyll站点
date: 2023-04-17 15:30:00 +0800
categories: web
tags: [jekyll, amplify]
excerpt: 本文介绍在AWS的Amlify中部署Jekyll站点的详细步骤。
---
　　<a href="https://aws.amazon.com/cn/amplify/" target="_blank">Amplify</a>是大名鼎鼎的AWS提供的Web和移动应用程序托管服务，按实际使用量付费，并提供12个月的免费试用(注意存储、构建和访问量过大可能会产生额外收费，<a href="https://aws.amazon.com/cn/amplify/pricing/" target="_blank">详见定价页面</a>)。本文介绍使用Amplify部署Jekyll站点的方法。

## 一、进入AWS Amplify
　　登录<a href="https://console.aws.amazon.com/console/" target="_blank">AWS的控制台</a>，点击顶部右侧的“区域”，选择一个你喜欢的服务器地点。建议选择离我们地理距离相对较近的东京、香港、新加坡。实际测试中发现Amplify是提供CDN的，所以也不必太纠结选择哪个区域。
　　在顶部的搜索框中搜索“Amplify”，点击第一个搜索结果“AWS Amplify”，进入Amplify的控制台。接着滑动到页面的最下方，找到“托管Web应用程序”，点击“开始使用”。

## 二、指定代码仓
　　Amplify支持直接从GitHub、GitLab获取代码，也支持孤陋寡闻的阿猪从没听说过的BitBucket和AWS CodeCommit，还支持直接上传代码。
　　我们这里选择从自己的GitHub代码仓获取代码。点击“GitHub”，然后点击“继续”，跳转到GitHub的授权页面。
　　在授权页面中点击右下角绿色的“Authorize AWS Amplify”按钮，跳转到GitHub的“Install & Authorize AWS Amplify”页面，根据需要选择将GitHub账户中的哪些代码仓授权给Amplify。你可以只选择存放Jekyll的代码仓，也可以选择全部代码仓。选择好后，点击页面底部的“Install&Authorize”按钮。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-01.jpg" alt="" width="400" height=auto/></div>

## 三、构建设置
　　授权成功后，页面会跳转回Amplify，进入构建(CI/CD)设置页面。
　　第1步是“添加存储库分支”，这里根据你的实际情况选择。一般是使用main分支。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-02.jpg" alt="" width="512" height=auto/></div>

　　第2步是配置用于构建页面和部署的脚本。Amplify会自动识别出应用的类型是Jekyll，并提供对应的预设脚本。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-03.jpg" alt="" width="512" height=auto/></div>

　　但是经过测试，阿猪发现这个预设脚本无法正常完成部署任务，日志中会出现如下错误信息：
```
[WARNING]: /usr/local/rvm/rubies/ruby-2.7.6/lib/ruby/2.7.0/bundler/resolver.rb:290:in `block in verify_gemfile_dependencies_are_found!': Could not find gem 'jekyll-sitemap' in any of the gem sources listed in your Gemfile. (Bundler::GemNotFound)`
```
　　按照错误信息的提示，大致是因为`jekyll-sitemap`这个gem没有被正确的安装。需要在执行构建任务之前先升级RubyGems到3.3.22以上版本，然后执行`bundle install`命令，以确保Amplify环境安装了部署Jekyll所必需的gems。以下是修改后的完整代码：

```
version: 1
frontend:
  phases:
    preBuild:
      commands:
        - gem install bundler
        - bundle install
    build:
      commands:
        - jekyll b
  artifacts:
    baseDirectory: _site
    files:
      - '**/*'
  cache:
    paths: []
```

　　第3步是审核，直接点击页面中的“保存并部署”按钮即可。

## 四、构建和部署
　　接下来Amplify会开始执行构建和部署，我们可以在页面中看到状态进度。
　　执行完成后，会看到下图所示的三个绿色对勾。这个时候Jekyll站点已经部署完成了，点击下图红框位置的链接即可正常访问。这个链接使用的是Amplify提供的免费二级域名。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-04.jpg" alt="" width="512" height=auto/></div>

## 五、绑定自己的域名
　　如果你希望绑定自己的域名，可以参考下边的操作：
　　点击Amplify控制台左侧的“域管理”，进入“域管理”界面，然后点击右上角的“添加域”按钮。在“添加域”界面的搜索框中填入自己的域名，然后点击“配置域”按钮。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-05.jpg" alt="" width="512" height=auto/></div>
　　这里看个人的喜好，如果喜欢使用www.example.com形式的网址，那么不需要做任何改动，直接点击“保存”按钮即可。如果不喜欢使用www前缀，可以稍后在Amplify控制台左侧的“重写和重定向”中修改重定向记录，将`https://www.example.com`302重定向至`https://example.com`。

　　Amplify强制使用HTTPS，并且免费提供Amazon自家的SSL证书。安装SSL证书的过程是自动化的，我们只需要确保域名可以正常解析就行了。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-06.jpg" alt="" width="512" height=auto/></div>
　　首先是添加一条CNAME记录验证你对域名的所有权。登录到你的域名解析管理后台，按照上图所示黑框中的要求添加一条CNAME记录。
<table align="center">
  <tr>
    <td>记录类型</td>
    <td>CNAME</td>
  </tr>
  <tr>
    <td>主机记录</td>
    <td>.example.com之前的字符串</td>
  </tr>
  <tr>
    <td>记录值</td>
    <td>CNAME之后的字符串</td>
  </tr>
</table>
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-07.jpg" alt="" width="512" height=auto/></div>
　　接着耐心等待Amplify验证，并将SSL配置信息记录同步给AWS全球的各个节点。这可能需要 30 分钟。完成后会看到我们的自定义域名变为了可用状态。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-08.jpg" alt="" width="512" height=auto/></div>
　　现在Amplify这边的操作已经完成了，还差最后一步添加DNS解析记录。按照Amplify的要求，我们需要为域名添加一条ANAME记录，如下图所示。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-09.jpg" alt="" width="512" height=auto/></div>
　　如果你的域名解析服务提供商不支持ANAME记录，你可以将你的域名解析服务提供商改成Amazon，这里不再展开介绍。也可以直接使用CNAME记录替代，登录你的域名解析管理后台，添加一条CNAME记录：
<table align="center">
  <tr>
    <td>记录类型</td>
    <td>CNAME</td>
  </tr>
  <tr>
    <td>主机记录</td>
    <td>@</td>
  </tr>
  <tr>
    <td>记录值</td>
    <td>&#60;替换为你的记录值&#62;.cloudfront.net</td>
  </tr>
</table>

　　如果你同时使用www前缀，需要再添加一条对应的CNAME记录：
<table align="center">
  <tr>
    <td>记录类型</td>
    <td>CNAME</td>
  </tr>
  <tr>
    <td>主机记录</td>
    <td>www</td>
  </tr>
  <tr>
    <td>记录值</td>
    <td>&#60;替换为你的记录值&#62;.cloudfront.net</td>
  </tr>
</table>

　　域名解析记录一般会即刻生效，但是受网络节点中的DNS缓存和本地DNS缓存、浏览器缓存的影响，可能有几分钟甚至更久的延迟。你可以尝试刷新本地DNS缓存、清空浏览器缓存，然后通过在浏览器中查看SSL证书信息来判断你的Jekyll站点是否已经托管在Amplify。
<div align="center"><img src="/post_img/2023/04/deploy-jekyll-on-amplify-10.jpg" alt="" width="512" height=auto/></div>