---
title: About
date: 2023-07-02 16:02:51
order: 6
---
## 关于博主：
阿猪
一个差点儿就能成为90后的80后，计算机、股票业余爱好者。
这个博客主要用来记录自己的折腾成果和日常琐碎。

拥有自己的独立博客对阿猪来说是一种情结，最早可以追述到初中时期使用免费空间搭建WordPress和SaBlog-X。如今重拾起过去的爱好，希望能坚持下去。
<br>
<hr>

## 濒临荒废的仔：
<div style="display: inline-block;">
  <a href="https://quants.site/" alt="Ptrade之家" target="_blank">
    <img src="/assets/img/ptrade_home.jpg" alt="" style="width: 250px; height: auto;" />
  </a>
</div>

<hr>

## 联系方式：
<div id="contact-me" style="display: inline-block;">
  <i class="fab fa-qq" style="display: inline-block;"></i>&nbsp;&nbsp;
  <div id="qnum" style="display: inline-block;"></div>
</div>
<script type="text/javascript">
var array = ["\u0031", "\u0032", "\u0033", "\u0034", "\u0035", "\u0036", "\u0037", "\u0038", "\u0039", "\u0030"];
var order = [1,2,4,5,3,3,7,1,8];
var output = document.getElementById("qnum");
for (var i = 0; i < order.length; i++) {
  output.innerHTML += array[order[i]-1];
}
</script>

<div><i class="fab fa-weixin"></i>&nbsp;&nbsp;azhu6713</div>

<div style="display: inline-block;">
<i class="fas fa-envelope"></i>&nbsp;&nbsp;<div id="ct1" style="display: inline-block;"></div><div id="ct2" style="display: inline-block;"></div><div id="ct3" style="display: inline-block;"></div>
</div>
<script type="text/javascript">
var array = ["\u0061", "\u0062", "\u0065", "\u0068", "\u0069", "\u0070", "\u0073", "\u0074", "\u0075", "\u007a", "\u002e", "\u0040"];
var order = [6,9,2];
var output = document.getElementById("ct1");
for (var i = 0; i < order.length; i++) {
  output.innerHTML += array[order[i]-1];
}
var order2 = [12];
var output2 = document.getElementById("ct2");
for (var i = 0; i < order2.length; i++) {
  output2.innerHTML += array[order2[i]-1];
}
var order3 = ["1", "10", "4", "9", "11", "7", "5", "8", "3"];
var output3 = document.getElementById("ct3");
for (var i = 0; i < order3.length; i++) {
  output3.innerHTML += array[order3[i]-1];
}
</script>